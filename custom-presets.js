const
	util      = require( 'tempaw-functions' ).util,
	action    = require( 'tempaw-functions' ).action;

let preset = {};

/**
 * Обновление шаблона в Novi билдере из dev версии (Вброс в билдер)
 * @param {object}  data
 * @param {string}  data.dev
 * @param {string}  data.builder
 * @param {string}  [data.marker]     - имя маркера удаления
 * @param {string}  [data.backup]     - путь к папке для бекапа важных файлов билдера (media, project.json, elements )
 * @param {string}  [data.tmp]        - путь к временной папке (промежуточная папка для обработки копированных из dev файлов)
 * @param {Array}   [data.pages]      - список страниц для вброса в билдер (остальные проигнорируются)
 * @param {boolean} [data.srcReplace] - замена исходных файлов Novi проекта в dev
 * @param {boolean} [data.debug]      - отладочные логи
 * @param {string}  [data.task]       - отображаемое имя задачи
 * @return {object} - правило
 */
preset.builderThrow = function ( data ) {
	if ( !data || !data.dev || !data.builder ) throw Error( 'Required parameter of "builder-throw" not specified (dev, builder)' );
	if ( !data.marker ) data.marker = 'BUILDER';
	if ( !data.backup ) data.backup = 'sources/backup-novi';
	if ( !data.tmp )    data.tmp    = 'tmp';
	if ( !data.pages )  data.pages = '!(_)*.html';
	else if ( data.pages instanceof Array ) data.pages = '*('+ data.pages.join('|') +').html';

	data.execute = util.genBuildTask( function () {
		let ruleSet = [];

		// TODO Предварительная проверка структуры dev, проверка существования нужных pug файлов
		// TODO Перевести все на английский

		ruleSet = ruleSet.concat([
			// Бекап важных файлов
			action.clean({ src: [ `${data.backup}/project.json`, `${data.backup}/novi`, `${data.backup}/elements` ] }),
			action.copy({ src: `${data.builder}/projects/template/project.json`, dest: `${data.backup}/` }),
			action.copy({ src: `${data.builder}/projects/template/novi/**/*`, dest: `${data.backup}/novi/` }),
			action.copy({ src: `${data.builder}/projects/template/elements/**/*`, dest: `${data.backup}/elements/` }),

			// Зачистка шаблона в билдере
			action.clean({ src:`${data.builder}/projects/template/components` }),
			action.clean({ src:`${data.builder}/projects/template/images` }),
			action.clean({ src:`${data.builder}/projects/template/video` }),
			action.clean({ src:`${data.builder}/projects/template/audio` }),
			action.clean({ src:`${data.builder}/projects/template/*.html` }),

			// Замена файлов шаблона
			action.copy({
				src: [
					`${data.dev}/**/*.html`,
					`${data.dev}/**/*.css`,
					`${data.dev}/**/*.js`,
					`${data.dev}/**/*.otf`,
					`${data.dev}/**/*.eot`,
					`${data.dev}/**/*.svg`,
					`${data.dev}/**/*.ttf`,
					`${data.dev}/**/*.woff`,
					`${data.dev}/**/*.woff2`,
					`${data.dev}/**/*.png`,
					`${data.dev}/**/*.jpg`,
					`${data.dev}/**/*.gif`,
					`${data.dev}/**/*.ico`,
					`${data.dev}/**/*.php`,
					`${data.dev}/**/*.tpl`,
					`${data.dev}/**/*.json`,
					`${data.dev}/**/*.txt`,
					`${data.dev}/**/*.mp4`
				],
				dest:`${data.builder}/projects/template/`
			}),

			// Copy files to a temporary folder
			action.copy({
				src: [
					`${data.dev}/**/*.pug`,
					`${data.dev}/**/*.scss`,
					`${data.dev}/**/*.js`
				],
				dest: 'tmp'
			}),

			// Deleting code fragments
			action.delMarker({
				src: [
					`${data.tmp}/**/*.pug`,
					`${data.tmp}/**/*.scss`,
					`${data.tmp}/**/*.js`
				],
				dest: 'tmp',
				marker: data.marker
			}),

			// Compile sass
			action.sass({
				src: `${data.tmp}/**/*.scss`,
				dest: `${data.builder}/projects/template/`,
				autoprefixer: false
			}),

			// Compile pug
			action.pug({
				src: `${data.tmp}/pages/!(_)*.pug`,
				dest: `${data.tmp}/`
			}),

			// Copy js files
			action.copy({
				src: `${data.tmp}/**/*.js`,
				dest: `${data.builder}/projects/template/`
			}),

			// Копирование в билдер только необходимых страниц
			action.clean({ src: `${data.builder}/projects/template/*.html` }),
			action.copy({ src: `${data.tmp}/${data.pages}`, dest: `${data.builder}/projects/template/` }),

			// Удаление мусора из разметки страниц (data-preset)
			action.htmlAsJson({
				src: `${data.builder}/projects/template/*.html`,
				task: `Remove "data-preset" attributes from pages`,
				callback: function (json) {
					if (json.node === 'element' && json.attr && json.attr.hasOwnProperty('data-preset')) {
						delete json.attr['data-preset'];
					}
				}
			}),

			// Регенерация страниц
			action.genPages({ builder: data.builder, debug: data.debug }),

			// Регенерация пресетов
			action.clean({ src: `${data.builder}/projects/template/elements/*.html` }),
			action.genPresets({ html: `${data.tmp}/*.html`, builder: data.builder, debug: data.debug }),

			// Удаление папки tmp
			action.clean({ src: data.tmp })
		]);

		if ( data.srcReplace ) {
			ruleSet = ruleSet.concat([
				// Замена исходных файлов
				action.clean({ src: [ `${data.dev}/project.json`, `${data.dev}/novi`, `${data.dev}/elements` ] }),
				action.copy({ src: `${data.builder}/projects/template/project.json`, dest: `${data.dev}/` }),
				action.copy({ src: `${data.builder}/projects/template/novi/**/*`, dest: `${data.dev}/novi/` }),
				action.copy({ src: `${data.builder}/projects/template/elements/**/*`, dest: `${data.dev}/elements/` })
			]);
		}

		return ruleSet;
	}());

	data.execute.displayName = data.task || 'Throw in Novi';
	return data;
};

module.exports = preset;
