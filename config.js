const
	action = require( 'tempaw-functions' ).action,
	preset = require( './custom-presets' );

module.exports = {
	livedemo: {
		enable: true,
		server: {
			baseDir: `dev/`,
			directory: false
		},
		port: 8000,
		open: false,
		notify: true,
		reloadDelay: 0,
		ghostMode: {
			clicks: false,
			forms: false,
			scroll: false
		}
	},
	sass: {
		enable: true,
		showTask: false,
		watch: `dev/**/*.scss`,
		source: `dev/**/!(_)*.scss`,
		dest: `dev/`,
		options: {
			outputStyle: 'expanded',
			indentType: 'tab',
			indentWidth: 1,
			linefeed: 'cr'
		}
	},
	pug: {
		enable: true,
		showTask: false,
		watch: `dev/**/*.pug`,
		source: `dev/pages/!(_)*.pug`,
		dest: `dev/`,
		options: {
			pretty: true,
			verbose: true,
			self: true,
			emitty: true
		}
	},
	autoprefixer: {
		enable: false,
		options: {
			cascade: true,
			browsers: ['Chrome >= 45', 'Firefox ESR', 'Edge >= 12', 'Explorer >= 10', 'iOS >= 9', 'Safari >= 9', 'Android >= 4.4', 'Opera >= 30']
		}
	},
	watcher: {
		enable: true,
		watch: `dev/**/*.js`
	},
	lint: {
		showTask: true,
		sass: 'dev/components/!(bootstrap)/**/*.scss',
		pug: 'dev/**/*.pug',
		js: 'dev/**/!(*.min).js',
		html: 'dev/**/*.html'
	},
	buildRules: {
		// Project build recommendation
		// Gulp Settings >> Node options >> --max-old-space-size=4096
		'Build Dist': [
			// Clean public
			action.clean({ src: 'public' }),

			// Copy files to a temporary folder
			action.copy({
				src: [
					'dev/**/*.pug',
					'dev/**/*.scss',
					'dev/**/*.js'
				],
				dest: 'tmp'
			}),

			// Deleting code fragments
			action.delMarker({
				src: [
					'tmp/**/*.pug',
					'tmp/**/*.scss',
					'tmp/**/*.js'
				],
				dest: 'tmp',
				marker: 'DIST'
			}),

			// LIVEDEMO

			// Compile sass
			action.sass({
				src: 'tmp/**/*.scss',
				dest: 'public/livedemo',
				autoprefixer: false
			}),

			// Compile pug
			action.pug({
				src: [
					`tmp/pages/!(_)*.pug`
				],
				dest: 'public/livedemo',
				autoprefixer: false
			}),

			// Copy js files
			action.copy({
				src: 'tmp/**/*.js',
				dest: 'public/livedemo'
			}),

			// Copy fonts
			action.copy({
				src: [
					'dev/**/*.otf',
					'dev/**/*.eot',
					'dev/**/*.svg',
					'dev/**/*.ttf',
					'dev/**/*.woff',
					'dev/**/*.woff2'
				],
				dest: 'public/livedemo'
			}),

			// Copy & minify images
			action.minifyimg({
				src: [
					'dev/**/*.png',
					'dev/**/*.jpg',
					'dev/**/*.gif'
				],
				dest: 'public/livedemo'
			}),

			// Copy other files
			action.copy({
				src: [
					'dev/**/*.ico',
					'dev/**/*.php',
					'dev/**/*.json',
					'dev/**/*.txt'
				],
				dest: 'public/livedemo'
			}),

			// GRANTER

			action.copy({
				src: [
					'dev/**/*.pug',
					'dev/**/*.html',
					'dev/**/*.scss',
					'dev/**/*.css',
					'dev/**/*.js',
				],
				dest: 'public/granter/dev'
			}),

			// Copy fonts
			action.copy({
				src: [
					'dev/**/*.otf',
					'dev/**/*.eot',
					'dev/**/*.svg',
					'dev/**/*.ttf',
					'dev/**/*.woff',
					'dev/**/*.woff2'
				],
				dest: 'public/granter/dev'
			}),

			// Copy & minify images
			action.minifyimg({
				src: [
					'dev/**/*.png',
					'dev/**/*.jpg',
					'dev/**/*.gif'
				],
				dest: 'public/granter/dev'
			}),

			// Copy other files
			action.copy({
				src: [
					'dev/**/*.ico',
					'dev/**/*.php',
					'dev/**/*.json',
					'dev/**/*.txt'
				],
				dest: 'public/granter/dev'
			}),

			// Copy project files
			action.copy({
				src: [
					'config.js',
					'custom-presets.js',
					'gulpfile.js',
					'package.json'
				],
				dest: 'public/granter/'
			}),

			// TF

			// TF livedemo (public)
			// Compile sass
			action.sass({
				src: 'tmp/**/*.scss',
				dest: 'public/user-package/public',
				autoprefixer: false
			}),

			// Compile pug
			action.pug({
				src: [
					`tmp/pages/!(_)*.pug`
				],
				dest: 'public/user-package/public',
				autoprefixer: false
			}),

			// Copy js files
			action.copy({
				src: 'tmp/**/*.js',
				dest: 'public/user-package/public'
			}),

			// Copy fonts
			action.copy({
				src: [
					'dev/**/*.otf',
					'dev/**/*.eot',
					'dev/**/*.svg',
					'dev/**/*.ttf',
					'dev/**/*.woff',
					'dev/**/*.woff2'
				],
				dest: 'public/user-package/public'
			}),

			// Copy & minify images
			action.copy({
				src: [
					'dev/**/*.png',
					'dev/**/*.jpg',
					'dev/**/*.gif'
				],
				dest: 'public/user-package/public'
			}),

			action.imgPlaceholder({ // Замена картинок на плейсхолдеры
				src: function() {
					var exclusions = [
						'_blank',
						'gmap*',
						'logo*',
						'sprite*',
						'warning_bar_0000_us',
						'isotope-loader',
						'mCSB_buttons',
						'preloader',
						'chrome-59x57',
						'edge-59x59',
						'firefox-59x60',
						'safari-59x57',
						'video-play',
						'vimeo-play',
						'youtube-play'
					];
					return `dist/user-package/dist/images/**/!(${exclusions.join('|')}).@(png|jpg)`;
				}()
			}),

			// Copy other files
			action.copy({
				src: [
					'dev/**/*.ico',
					'dev/**/*.php',
					'dev/**/*.json',
					'dev/**/*.txt'
				],
				dest: 'public/user-package/public'
			}),

			// TF dev
			action.copy({
				src: [
					'dev/**/*.pug',
					'dev/**/*.html',
					'dev/**/*.scss',
					'dev/**/*.css',
					'dev/**/*.js',
				],
				dest: 'public/user-package/dev'
			}),

			// Copy fonts
			action.copy({
				src: [
					'dev/**/*.otf',
					'dev/**/*.eot',
					'dev/**/*.svg',
					'dev/**/*.ttf',
					'dev/**/*.woff',
					'dev/**/*.woff2'
				],
				dest: 'public/user-package/dev'
			}),

			// Copy & minify images
			action.copy({
				src: [
					'dev/**/*.png',
					'dev/**/*.jpg',
					'dev/**/*.gif'
				],
				dest: 'public/user-package/dev'
			}),

			action.imgPlaceholder({ // Замена картинок на плейсхолдеры
				src: function() {
					var exclusions = [
						'_blank',
						'gmap*',
						'logo*',
						'sprite*',
						'warning_bar_0000_us',
						'isotope-loader',
						'mCSB_buttons',
						'preloader',
						'chrome-59x57',
						'edge-59x59',
						'firefox-59x60',
						'safari-59x57',
						'video-play',
						'vimeo-play',
						'youtube-play'
					];
					return `dist/user-package/dev/images/**/!(${exclusions.join('|')}).@(png|jpg)`;
				}()
			}),

			// Copy other files
			action.copy({
				src: [
					'dev/**/*.ico',
					'dev/**/*.php',
					'dev/**/*.json',
					'dev/**/*.txt'
				],
				dest: 'public/user-package/dev'
			}),

			// Documentation
			action.copy({
				src: [
					'sources/documentation/**/*.html',
					'sources/documentation/**/*.css',
					'sources/documentation/**/*.js',
					'sources/documentation/**/*.otf',
					'sources/documentation/**/*.eot',
					'sources/documentation/**/*.svg',
					'sources/documentation/**/*.ttf',
					'sources/documentation/**/*.woff',
					'sources/documentation/**/*.woff2',
					'sources/documentation/**/*.png',
					'sources/documentation/**/*.jpg',
					'sources/documentation/**/*.gif',
					'sources/documentation/**/*.ico'
				],
				dest: 'public/user-package/documentation'
			}),

			// Copy project files
			action.copy({
				src: [
					'config.js',
					'custom-presets.js',
					'gulpfile.js',
					'package.json'
				],
				dest: 'public/user-package/'
			}),

			// Delete temporary folder
			action.clean({ src: 'tmp' })
		],
		'To Builder': [
			preset.builderThrow({
				dev: 'dev',
				builder: 'builder',
				pages: 'index.html',
				debug: true
			})
		]
	}
};
